package com.ust.dao;

import java.util.List;

import com.ust.model.ItemType;

public interface ItemDao {
    boolean insertItemDetail(ItemType itm);
	
	List<ItemType> getAllItemDetails();

}
