package com.ust.arrayexample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.ust.dao.ItemDaoImp;
import com.ust.model.ItemType;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	ItemDaoImp dimp= new ItemDaoImp();
    	String str="";
    	int i=1;
    	do {
    		
    		System.out.println("Enter the details of Type "+i);
    		ItemType items=new ItemType();
    		i++;
    		System.out.println("Name:");
			items.setName(reader.readLine());
			
			System.out.println("Deposit:");
			items.setDeposit(Double.parseDouble(reader.readLine()));
			
			System.out.println("Cost per Day:");
			items.setCostPerDay(Double.parseDouble(reader.readLine()));
			boolean status = dimp.insertItemDetail(items);
			if (status) {
				System.out.println("Data Added");
			} else {
				System.out.println("Data Not Added");
			}
			

    		System.out.println("Do You Want to continue?(y/n)");
			str=reader.readLine();

    	}while(str.equalsIgnoreCase("y"));
    	List<ItemType> list = dimp.getAllitemDetails();
    	
    	
		if(list.isEmpty()) {
			System.out.println("Item List is Empty");
		}
		else {
			System.out.println("Name    Deposit   Cost Per Day");
			for (ItemType item : list) {
				
				System.out.println(item);
			}	
		}
        

    }
}
