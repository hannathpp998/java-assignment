package com.ust.dao;

import java.util.HashMap;
import java.util.Map;

import com.ust.model.Client;

public class ClientdaoImp implements Clientdao{
	Map<String, Client> clientMap = new HashMap<String, Client>();

	public boolean insertClientDetails(String passportNumber, Client client) {
		Client cli = clientMap.put(passportNumber, client);
		if (cli!=null) {
			return true;
		}
		return false;
	}

	public Map<String, Client> getAllClientDetails() {
		return clientMap;
	}

}
