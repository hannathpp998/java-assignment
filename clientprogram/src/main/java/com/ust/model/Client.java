package com.ust.model;

public class Client {
	private String clientName;
	private String email;
	private String passportNumber;
	
	public Client() {
		super();
		
	}
	public Client(String clientName, String email, String passportNumber) {
		super();
		this.clientName = clientName;
		this.email = email;
		this.passportNumber = passportNumber;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	
}
