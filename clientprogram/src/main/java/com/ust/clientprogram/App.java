package com.ust.clientprogram;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Scanner;

import com.ust.dao.ClientdaoImp;
import com.ust.model.Client;


public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
    	ClientdaoImp dimp = new ClientdaoImp();
    	System.out.println("Enter the number of clients:");
    	Scanner sc=new Scanner(System.in);
    	int n=sc.nextInt();
    	int c = 1;
    	for(int i=1;i<=n;i++) {
    		System.out.println("Enter the details of the client  "+c);
    		Client client=new Client();
    		client.setClientName(reader.readLine());
    		client.setEmail(reader.readLine());
    		String passportNumber =reader.readLine();
    		boolean status=dimp.insertClientDetails(passportNumber, client);
    		c++;
    			
    	}
    	Map<String, Client> cmap = dimp.getAllClientDetails();
		System.out.println("Enter the passport number of the client to be searched ");
		String searchKey=reader.readLine();
		System.out.println("Client Details");
		if(cmap.containsKey(searchKey)) {
			Client cmp = cmap.get(searchKey);
			System.out.println(cmp.getClientName() +"--" + cmp.getEmail()+"--"+ searchKey);
		}
		else {
			System.out.println("Employee is not found");
		}

    	sc.close();
    	
    }
}
